import cn from 'classnames';
import st from './style.module.scss';
import { PipButtonProps, validatePipButtonProps, PipButtonDefaultProps } from './PipButtonProps';
import { variantStyles } from './PipButtonConstants';

export const PipButton = ({ children, variant, category }) => {
    const error = validatePipButtonProps({ children, variant, category });
    if (error) {
        console.error(error);
        return null;
    }
    const classes = cn(st['pip-b'], variantStyles[variant]);

    return <button className={classes}>{children}</button>;
};

PipButton.propTypes = PipButtonProps;
PipButton.defaultProps = PipButtonDefaultProps;
