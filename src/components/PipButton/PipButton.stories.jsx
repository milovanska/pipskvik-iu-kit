import { PipButton } from './PipButton';

import { variantIncludes } from './PipButtonConstants';

export default {
    title: 'UI/PipButton',
    component: PipButton,
    argTypes: {
        variant: {
            type: 'string',
            description: 'Вариант отображения внешнего вида',
            control: { type: 'radio' },
            options: variantIncludes,
        },
    },
};

const Template = (args) => <PipButton {...args} />;

export const Default = Template.bind({});
Default.args = {
    children: 'Default btn',
    variant: 'default',
};

export const Confirm = Template.bind({});
Confirm.args = {
    children: 'Confirm btn',
    variant: 'confirm',
};

export const Danger = Template.bind({});
Danger.args = {
    children: 'Danger btn',
    variant: 'danger'
};

export const Success = Template.bind({});
Success.args = {
    children: 'Success btn',
    variant: 'success'
};

export const Info = Template.bind({});
Info.args = {
    children: 'Info btn',
    variant: 'info'
};
