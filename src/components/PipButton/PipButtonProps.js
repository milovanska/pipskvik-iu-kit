import PropTypes from 'prop-types';
import { variantIncludes } from './PipButtonConstants';

const PipButtonProps = {
	children: PropTypes.node.isRequired,
	variant: PropTypes.oneOf(variantIncludes),
	category: PropTypes.string,
};

const validatePipButtonProps = (props) => {
	const { variant } = props;
	
	if (variant && !variantIncludes.includes(variant)) {
		return new Error(`Invalid prop 'variant' supplied. It should be one of: ${variantIncludes}`);
	}

	return null;
};

const PipButtonDefaultProps = {
	variant: 'default',
	category: null,
};

export {
	PipButtonProps,
	validatePipButtonProps,
	PipButtonDefaultProps
};