import st from './style.module.scss';

const variantIncludes = ['default', 'confirm', 'danger', 'success', 'info'];

const variantStyles = {
    confirm: st['pip-b--confirm'],
    danger: st['pip-b--danger'],
    success: st['pip-b--success'],
    info: st['pip-b--info'],
};

export { variantIncludes, variantStyles };